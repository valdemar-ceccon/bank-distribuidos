from dataclasses import dataclass
from enum import Enum
from typing import Dict
from typing import List
from typing import Optional
from typing import Tuple
from typing import Union

from flask import Flask
from flask import g
from flask_restful import Api
from flask_restful import reqparse
from flask_restful import Resource


class Operacoes(Enum):
    DEPOSITO = "deposito"
    SAQUE = "saque"


@dataclass
class Conta:
    id: int
    nome: str
    cpf: str
    saldo: float


class ContasIF(Resource):
    def __init__(self) -> None:
        super().__init__()
        self.contas: Contas = Contas()

    def __parse_operacao(self) -> Tuple[str, float]:
        parser = reqparse.RequestParser()
        parser.add_argument("op", type=str)
        parser.add_argument("valor", type=float)
        args = parser.parse_args()
        return args["op"], args["valor"]

    def __parse_dados_cliente(self) -> Tuple[str, str]:
        parser = reqparse.RequestParser()
        parser.add_argument("nome")
        parser.add_argument("cpf")
        args = parser.parse_args()
        return args["nome"], args["cpf"]

    def get(self, id: int = None, cpf: str = None) -> List[Conta]:
        if id or cpf:
            return self.contas.find_contas(id, cpf).__dict__

        return [x.__dict__ for x in self.contas.list_contas()]

    def patch(self, id: int) -> Dict:
        op, valor = self.__parse_operacao()

        if op == Operacoes.DEPOSITO.value:
            return self.contas.deposito(id, valor).__dict__
        if op == Operacoes.SAQUE.value:
            return self.contas.saque(id, valor).__dict__

        return {"message": f"Error: operacao não encontrada {op}"}

    def post(self) -> Union[str, int]:
        nome, cpf = self.__parse_dados_cliente()
        return self.contas.criar_contas(nome=nome, cpf=cpf)

    def delete(self, id: int) -> str:
        return self.contas.delete_conta(id)


class Contas:
    db: List[Conta] = []
    last_id: int = 0

    def find_contas(self, id: int = None, cpf: str = None) -> Optional[Conta]:
        if id:
            conta = [c for c in Contas.db if c.id == id]
        elif cpf:
            conta = [c for c in Contas.db if c.cpf == cpf]

        if conta:
            return conta[0]

        return None

    def list_contas(self) -> List[Conta]:
        return list(Contas.db)

    def deposito(self, id: int, valor: float) -> Union[str, Conta]:
        conta = self.find_contas(id)

        if not conta:
            return "conta nao existe"

        conta.saldo += valor

        return conta

    def saque(self, id: int, valor: float) -> Union[str, Conta]:
        conta = self.find_contas(id=id)

        if not conta:
            return "conta nao existe"

        conta.saldo -= valor

        return conta

    def criar_contas(self, nome: str, cpf: str) -> Union[str, int]:
        if self.find_contas(cpf=cpf):
            return "cpf ja cadastrado"
        Contas.last_id += 1
        Contas.db.append(Conta(Contas.last_id, nome, cpf, 0))

        return Contas.db[-1].id

    def delete_conta(self, id) -> str:
        idxs = [idx for idx, conta in enumerate(Contas.db) if conta.id == id]
        if idxs:
            del Contas.db[idxs[0]]
            return f"{id} deleted"
        return f"{id} nao existe"


def main():
    app = Flask(__name__)
    api = Api(app)
    api.add_resource(
        ContasIF, "/conta", "/contas", "/conta/id/<int:id>", "/conta/cpf/<string:cpf>"
    )
    app.run(debug=True)
    return 0


if __name__ == "__main__":
    raise SystemExit(main())
