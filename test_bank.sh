#!/usr/bin/env bash

echo CRIANDO CONTAS
curl -X POST -H 'Content-Type: application/json' http://localhost:5000/contas -d '{"nome": "valdemar", "cpf" : 1}'
curl -X POST -H 'Content-Type: application/json' http://localhost:5000/contas -d '{"nome": "gabriel", "cpf" : 2}'


echo TODAS AS CONTAS
curl -X GET http://localhost:5000/contas

echo DETALHES CADA CONTA
curl -X GET http://localhost:5000/contas/1
curl -X GET http://localhost:5000/contas/2

echo OPERACOES CONTA 1
curl -X PUT -H 'Content-Type: application/json' http://localhost:5000/contas/1 -d '{"op": "deposito", "valor": 500.0}'
curl -X PUT -H 'Content-Type: application/json' http://localhost:5000/contas/1 -d '{"op": "deposito", "valor": 1000.0}'

echo OPERACOES CONTA 2
curl -X PUT -H 'Content-Type: application/json' http://localhost:5000/contas/2 -d '{"valor": 1000.0, "op": "deposito"}'
curl -X PUT -H 'Content-Type: application/json' http://localhost:5000/contas/2 -d '{"op": "saque", "valor": 2.0}'
curl -X PUT -H 'Content-Type: application/json' http://localhost:5000/contas/2 -d '{"op": "saque", "valor": 3000.0}'

echo APAGA CONTAS
curl -X DELETE http://localhost:5000/contas/1
curl -X DELETE http://localhost:5000/contas/2

echo LISTA TODAS AS CONTAS
curl -X GET http://localhost:5000/contas
